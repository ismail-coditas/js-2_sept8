// lexical environment, scope chain
const lastName = "faizan";
const printName = function(){
    const firstName = "ismail";
    function myFunction(){
        console.log(firstName);
        console.log(lastName);
    }
    myFunction() 
}
printName();



