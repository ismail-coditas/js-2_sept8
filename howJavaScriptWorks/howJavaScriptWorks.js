//1 compilation 
console.log(this);
console.log(window);
console.log(firstName);
var firstName = "Faizan";
console.log(firstName);
// code execution
// global exection context
// local execution context  
// closures

//2 hoisting
console.log(this);
console.log(window);
console.log(myFunction);
console.log(fullName);
function myFunction(){
    console.log("this is my function");
}
var firstName = "Faizan";
var lastName = "Ismail"
var fullName = firstName + " " + lastName;
console.log(fullName);
console.log(myFunction);

// 3 function
var myFunction = function(){
    console.log("this is my function");
}
console.log(myFunction);

// 4 function expression
// Uncaught ReferenceError: 
// Cannot access 'firstName' before initialization
// Uncaught ReferenceError: 
// firstName is not defined
// console.log(firstName);
// console.log(firstName);
// let firstName;
// console.log(firstName);
// console.log(typeof firstName);
// let firstName = "Faizan";


// 5
console.log("hello world");
let firstName = "Faizan";
let lastName = "Ismail";
const myFunction = function() {
    let var1 = "First Variable";
    let var2 = "second Variable";
    console.log(var1);
    console.log(var2);
}