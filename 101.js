function func(){
    let counter = 0;
    return function(){
        if(counter < 1){
            console.log("Hi You Called me");
            counter++;
        }else{
            console.log("I already called once");
        }
    }
}
const myFunc = func();
myFunc();
myFunc();


